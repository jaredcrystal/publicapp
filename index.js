var express = require('express')
var app = express()
var mime = require('mime')
var bodyParser = require('body-parser');
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//mime.define({
  //  'text/html': ['byu', '.byu']
//});
//console.log(mime.lookup('byu'));

//app.engine('html', cons.swig)
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.sendfile('./public/index.html')
})

app.get('/info', function(req, res) {
  res.send(JSON.stringify(req.headers) + JSON.stringify(req.query));
})

app.post('/info', function(req, res) {
  res.send(JSON.stringify(req.headers) + JSON.stringify(req.query) + JSON.stringify(req.body));
})

app.get('/redirect', function(req, res) {
  if (req.query.hasOwnProperty('google')) {
    res.redirect('https://google.com');
  }
  else if (req.query.hasOwnProperty('facebook')) {
    res.redirect('https://facebook.com');
  }
  else {
    res.sendfile('./public/redirect.html')
  }
})

app.get('/version', function(req, res) {
  res.setHeader('content-type', 'application/json');
  if (req.headers['accept'] === 'application/vnd.byu.cs462.v1+json') {
    res.send('{"version": "v1" }')
  }
  else if (req.headers['accept'] === 'application/vnd.byu.cs462.v2+json') {
    res.send('{"version": "v2" }')
  }
  else {
    res.sendStatus(400);
  }
})

app.listen(3000, function () {
  console.log('listening on port 3000!')
})
